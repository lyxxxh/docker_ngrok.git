
### dockr ngrok搭建 
    git clione https://gitee.com/lyxxxh/docker_ngrok.git    

### 两处地方需要更改为你的域名
    build.sh
       export NGROK_DOMAIN="你的域名"
    start.sh
     ... -domain="你的域名" ...
     
     
### 开始安装
    1. docker build -t  ngrok .
    2. chmod +x  start.sh
    3. ./start.sh
    至此已经安装完成
    
### 获取客户端ngrok
    1. 进入容器:  docker exec -it 容器id  sh
    2. 进入ngrok目录: cd /ngrok/bin
    3. 打包: tar -czvf client.tar.gz darwin_386 darwin_amd64  linux_386 linux_arm windows_386 windows_amd64
    4. 回到宿主机: exit
    5. 将打包的复制到宿主机: docker cp  容器id:/ngrok/bin/client.tar.gz /
    
                 
